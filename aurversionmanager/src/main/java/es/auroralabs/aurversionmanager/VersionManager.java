package es.auroralabs.aurversionmanager;

/**
 * Created by franciscojosesotoportillo on 14/3/17.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class VersionManager {
    private static String TAG = "AURVersionManager";
    private int versionCode;
    private int previousVersionCode;
    private String currentVersionName;
    private String previousVersionName;
    private boolean isFirstRun = false;
    private boolean isAnUpdate = false;
    private static VersionManager instance;
    private SharedPreferences preferences;

    private static String PREVIOUS_VERSION_KEY = "vm_previous_version";
    private static String PREVIOUS_VERSION_CODE_KEY = "vm_previous_version_code";
    private static String CURRENT_VERSION_KEY = "vm_current_version";
    private static String CURRENT_VERSION_CODE_KEY = "vm_current_version_code";
    private static String FIRST_LAUNCH_KEY = "vm_first_launch";
    private static String LAUNCHES_COUNT_KEY = "vm_launches_count";
    private static String INSTALLED_VERSIONS_KEY = "vm_installed_versions";

    private VersionManager(Context context) {
        try {
            currentVersionName = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException nfe) {
            Log.e(TAG, "can't find version name");
        }
        try {
            versionCode = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException nfe) {
            Log.e(TAG, "can't find version code");
        }

        preferences = context.getSharedPreferences(
                TAG, Context.MODE_PRIVATE);


        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(CURRENT_VERSION_KEY, currentVersionName);
        editor.putInt(CURRENT_VERSION_CODE_KEY, versionCode);
        Date current = new Date();
        int launchesCount = preferences.getInt(LAUNCHES_COUNT_KEY, 0) + 1;
        editor.putInt(LAUNCHES_COUNT_KEY, launchesCount);
        previousVersionCode = preferences.getInt(CURRENT_VERSION_CODE_KEY, 0);
        previousVersionName = preferences.getString(CURRENT_VERSION_KEY, "");

        if (previousVersionName.equals("")) {
            isFirstRun = true;
            editor.putLong(FIRST_LAUNCH_KEY, current.getTime());
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(currentVersionName);
            editor.putString(INSTALLED_VERSIONS_KEY, jsonArray.toString());
        } else if (!previousVersionName.equals(currentVersionName)) {
            isAnUpdate = true;
            editor.putString(PREVIOUS_VERSION_KEY, previousVersionName);
            editor.putInt(PREVIOUS_VERSION_CODE_KEY, previousVersionCode);
            JSONArray jsonArray = null;
            try {
                jsonArray = new JSONArray(preferences.getString(INSTALLED_VERSIONS_KEY, "[]"));
            } catch (JSONException e) {
                Log.e(TAG, "can't load Installed versions");
            }

            if (jsonArray != null) {
                jsonArray.put(currentVersionName);
            }
            editor.putString(INSTALLED_VERSIONS_KEY, jsonArray.toString());
        }

        editor.commit();
    }

    public static VersionManager getInstance(Context context) {
        if (instance == null) {
            instance = new VersionManager(context);
        }

        return instance;
    }

    public String getCurrentVersionName() {
        if (preferences != null) {
            return preferences.getString(CURRENT_VERSION_KEY, "0.0");
        } else {
            return "0.0";
        }
    }

    public String getPreviousVersionName() {
        if (preferences != null) {
            return preferences.getString(PREVIOUS_VERSION_KEY, "0.0");
        } else {
            return "0.0";
        }
    }

    public boolean isFirstInstall() {
        return isFirstRun;
    }

    public boolean isAnUpdate() {
        return isAnUpdate;
    }

    public int getLaunchesCount() {
        if (preferences != null) {
            return preferences.getInt(LAUNCHES_COUNT_KEY, 0);
        } else {
            return 0;
        }
    }

    public List<String> getInstalledVersions() {
        List<String> installedArray = new ArrayList<>();
        if (preferences != null) {
            JSONArray jsonArray = null;
            try {
                jsonArray = new JSONArray(preferences.getString(INSTALLED_VERSIONS_KEY, "[]"));
            } catch (JSONException e) {
                Log.e(TAG, "can't load Installed versions");
            }

            if (jsonArray != null) {
                for (int i = 0 ; i < jsonArray.length() ; i++) {
                    try {
                        installedArray.add(jsonArray.getString(i));
                    } catch (JSONException jse) {
                        //nothing
                        Log.e(TAG, "Can't load Installed versions");
                    }
                }
            }

            return installedArray;
        } else {
            return installedArray;
        }
    }

    public Date getFirstLaunchDate() {
        if (preferences != null) {
           long time = preferences.getLong(FIRST_LAUNCH_KEY, 0);
           return new Date(time);
        } else {
            return null;
        }
    }
}
