package es.auroralabs.aurversionmanager.sample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.util.List;

import es.auroralabs.aurversionmanager.VersionManager;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        VersionManager vm = VersionManager.getInstance(this);

        if (vm.isFirstInstall()) {
            Log.d("LOG", "FIRST INSTALL");
            Log.d("LOG", vm.getFirstLaunchDate().toString());
        }

        if (vm.isAnUpdate()) {
            Log.d("LOG", "IS UPDATE");
        }

        Log.d("LOG", vm.getCurrentVersionName());
        Log.d("LOG", "{");

        List<String> array = vm.getInstalledVersions();

        for (int i = 0 ; i < array.size(); i++) {
            Log.d("LOG", array.get(i));
        }

        Log.d("LOG", "}");
    }
}
